import pickle, random, os, requests

from pokemontcgsdk import Card
from pokemontcgsdk import Set
from pokemontcgsdk import Type
from pokemontcgsdk import Supertype
from pokemontcgsdk import Subtype
from pokemontcgsdk import Rarity

def LookupCard(card_id):
    ## Check if the card has already been downloaded
    try:
        f = open("./card_cache/"+card_id+".card", "rb")
        #print("file exists, reading from file...")
        c = pickle.load(f)
        f.close()
    ## If it hasn't, find it in the API, then download it
    except:
        #print("file does not exist, reading from api, then creating file")
        c = Card.find(card_id)
        f = open("./card_cache/"+card_id+".card", "wb")
        pickle.dump(c, f)
        f.close()
        
        ## Saving image for GUI, comment out if not needed
        url = c.images.small
        img = requests.get(url)
        filename = c.id+".png"
        with open("./images/cards/"+filename, "wb") as f:
            f.write(img.content)
        ##################################################
    return c
    


#####################
## DECK OPERATIONS ##
    
def ReadDeck(name):
    try:
        f = open("./decks/"+name+".txt", "r")
        print("building deck...")
        deck = []
        for line in f.readlines():
            if line[0] == "#" or line.strip() == "":
                pass # Ignore comment lines
            else:
                if "#" in line:
                    line = line.split(" #")[0] # Ignore inline comments
                quantity, cardid = line.split(" ") # Quantity and ID separated by a space 
                quantity = quantity.replace("x", "") # Remove x from quantity
                cardid = cardid.replace("\n", "") # Remove newline from ID
                for x in range (0, int(quantity)):
                    deck.append(LookupCard(cardid))
        f.close()
        print(len(deck), "cards in deck")
        return deck
    except FileNotFoundError: ## If not using the tkinter GUI, user might mistype
        print("error: decklist not found")
    return None

## Recurring game mechanic :D
def FlipACoin():
    sides = ["Heads", "Tails"]
    return random.choice(sides)
