import random
from src.methods import ReadDeck

class Player:
    def __init__(self, player_name):
        self.name = player_name # TKINTER GAME ONLY WORKS IF NAMES ARE SET TO "Player 1" AND "Player 2" RESPECTIVELY
        # THIS MECHANIC HAS BEEN HARD CODED INTO THE GUI 
        
        self.deck_full = [] # This should only contain the full list of cards the player is using, not to be altered by gameplay
        self.deck = [] # List of cards
        self.hand = [] # List of cards
        self.prizes = [] # List of cards
        self.discard = [] # List of cards
        
        self.goFirst = False
        self.activeP = None # When set, is an object of PokemonInPlay
        self.bench = [] # List of PokemonInPlay objects (referred to as pip)
        
        self.turn = False
        self.action = {} # Used to keep track of actions being taken with cards
        # "Energy attachment" : energycard
        
    def BuildDeck(self, deck_name):
        self.deck_full = ReadDeck(deck_name)
        self.deck = list(self.deck_full) # copy value, not reference
        
    def StartingDraw(self):
        random.shuffle(self.deck)
        for x in range (0, 7):
            self.hand.append(self.deck[0])
            self.deck.remove(self.deck[0])
            
        self.PrintHand()
         
        self.MulliganCounter = 0
        while self.Mulligan():
            #print(self.Mulligan())
            self.MulliganCounter +=1
            #print("mulligan count:", self.MulliganCounter)
            
            self.hand = []
            self.deck = list(self.deck_full)
            #print("deck full count:", len(self.deck_full))
                
            random.shuffle(self.deck)
            for x in range (0, 7):
                self.hand.append(self.deck[0])
                self.deck.remove(self.deck[0])
        
        for x in range(0,6):
            self.prizes.append(self.deck[0])
            self.deck.remove(self.deck[0])
        
        print("HAND:", len(self.hand))
        #self.PrintHand()
        print("prize cards:", len(self.prizes))
        print("deck count:", len(self.deck))
        if self.MulliganCounter:
            print("MULLIGANS:", self.MulliganCounter)
            
    def ShuffleDeck(self):
        random.shuffle(self.deck)
        
    def DrawCards(self, draw):
        for x in range(0,draw):
            self.hand.append(self.deck[0])
            self.deck.remove(self.deck[0])
            
    def ShuffleHandIntoDeck(self):
        for card in self.hand:
            self.deck.append(card)
            self.hand.remove(card)
        self.ShuffleDeck()
            
    def Mulligan(self):
        for card in self.hand:
            if card.supertype == "Pokémon":
                if "Basic" in card.subtypes:
                    return False
        return True
        
    def PrintHand(self):
        for card in self.hand:
            print(card.name)
            
class PokemonInPlay:
    def __init__(self, card):
        self.card = card
        self.energy = []
        self.tool = None
