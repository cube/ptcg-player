# ptcg-player

Eventual PTCG simulator/tester, designed for offline use

**Under active development, nowhere near completion!**

Play against yourself / two (local) human players, or (eventually) play against a basic cpu, mainly with the aim of testing draw support, starting hand, energy availability, etc.

Designed with offline usage in mind, in order to allow for boredom play in areas without internet connection, like how the old Pokemon TCG CD game used to work :P Just make sure you have built the deck/s you want to use at least once before going offline.

~~Currently using the [Empotech (2008)](https://bulbapedia.bulbagarden.net/wiki/Empotech_(TCG)) as a base to work from.~~ Now using the [base set starter deck](https://bulbapedia.bulbagarden.net/wiki/2-Player_Starter_Set_(TCG)) to start from. Although both decks will be available, I am currently only testing with the base set deck from now on, so there may/will be glitches with any other deck, including Empotech.

Officially supported decks will always be stored in `/decks` as text files.

## Usage

1. clone the repo
2. open terminal within the ptcg-player directory
3. run `python main.py`
4. ???
5. profit

## Features

- read in any deck in the format described below
- draw a hand, evaluate for mulligan, draw prize cards
- cards are fetched from the api once, then stored in `/card_cache` as binary, so that the api isn't spammed on repetitive usage + for offline play

## GUI Features

- You can technically use any card "sleeves"/backs that you like, by changing `images/back.png` to anything you want (as long as the size is appropriate).
- Select both players' decks, and select who will decide the coin flip
- Opening coin flip
- Draw starting hand
- Select starting pokemon
- Attach energy to benched pokemon

## Structure

**Player class**: Stores information about the two players, e.g. their deck, hand, prize cards
**PokemonInPlay class**: Keeps track of Pokemon that are in play, and what cards are attached to them

## Deck List Format

- Lines starting with "#" are ignored, so are blank lines
- Only one space per line, between the quantity (e.g. 1x, 2x, 3x, 4x...) and the card id (except for inline comments)
- Card id in the format used with pokemontcgsdk until support added for ptcgo style
- Inline comments must start with a single space separating it from the cardid
- See "empotech.txt" in `/decks` for an example deck list

## TODO

(Priority 1/2/3, where 3 = unimportant and 1 = necessary)

- Deck validation (basic deck rules) (2)
- Rudimentary tk gui for readability (1)
- Config file for legacy/custom rules

## GUI TODO

- "Show hand" toggle for IRL players (3)

## Dream future

- Kivy GUI for desktop and mobile usage
- Custom format rule parsing

## Dependecies

- pokemontcgsdk
