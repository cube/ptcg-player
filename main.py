from src.classes import Player, PokemonInPlay
from src.methods import FlipACoin
import tkinter as tk
from tkinter import ttk
from tkinter.messagebox import showwarning
import os
from functools import partial

class Game():
    def __init__(self, root):
        ## Initialize the players
        self.Player1 = Player("Player 1")
        self.Player2 = Player("Player 2")
        
        ## Initialize global game variables
        self.LostZone = []
        self.Phase = "setup" # For game phase check when clicking cards
        
        ## Initialize card back image
        self.cardBackImage = tk.PhotoImage(file="./images/back.png")
        frameImageB = tk.PhotoImage(file="./images/frame.png")
        self.frameImage = frameImageB.subsample(3, 3)
        
        self.root = root
        self.root.title("ptcg-player")
        self.root.resizable(width=False, height=False)
        
        ## Variables for deck choice + coin flip decider
        dlist = self.GetAvailableDecks()
        self.coinChoiceVar = tk.StringVar()
        self.coinChoiceVar.set("Player 1")
        
        ## This log widget will write out every move/decision
        self.logBox = tk.Text(root, width = 25, height = 20)
        self.logBox.grid(row=0, column=0, rowspan=4, padx=5, pady=5)
        
        ## Player one's hand, active pokemon, choices, etc. go here
        self.player1Frame = tk.LabelFrame(root, text="Player 1")
        self.player1Frame.grid(row=0, column=1, padx=20, pady=20)
        
        ## Combobox for selecting deck (looks in /decks for .txt files)
        self.player1DeckChoose = ttk.Combobox(self.player1Frame, values = dlist)
        self.player1DeckChoose.set("Select a deck")
        self.player1DeckChoose["state"] = "readonly"
        self.player1DeckChoose.grid(row=0, column=0, padx=10, pady=10)
        
        ## Radio button choice for who should call the coin flip
        self.coinChoice1 = ttk.Radiobutton(self.player1Frame, text="Choose coin flip", value="Player 1", variable=self.coinChoiceVar)
        self.coinChoice1.grid(row=1, column=0, padx=10, pady=10)
        
        ## Player two's hand, active pokemon, choices, etc. go here
        self.player2Frame = tk.LabelFrame(root, text="Player 2")
        self.player2Frame.grid(row=1, column=1, padx=20, pady=20)
        
        ## Combobox for selecting deck (looks in /decks for files)
        self.player2DeckChoose = ttk.Combobox(self.player2Frame, values = dlist)
        self.player2DeckChoose.set("Select a deck")
        self.player2DeckChoose["state"] = "readonly"
        self.player2DeckChoose.grid(row=0, column=0, padx=10, pady=10)
        
        ## Radio button choice for who should call the coin flip
        self.coinChoice2 = ttk.Radiobutton(self.player2Frame, text="Choose coin flip", value="Player 2", variable=self.coinChoiceVar)
        self.coinChoice2.grid(row=1, column=0, padx=10, pady=10)
        
        ## Click here once everything is set up!
        self.startButton = tk.Button(root, text="Start", command = self.BuildPlayerDecks)
        self.startButton.grid(row=2, column=1, columnspan=2, padx=10, pady=10)
        
        ## Downloading is a blocking action. could introduce Threading but why
        self.noticeLabel = tk.Label(root, text="Please note that this window will hang\n while a deck is being downloaded for the first time.\n Do not force quit or it will stop!")
        self.noticeLabel.grid(row=3,column=1,padx=10,pady=10)
    
    ## Function for the combobox deck chooser
    def GetAvailableDecks(self):
        d = []
        for file in os.listdir("./decks/"): #Look here for files
            d.append(file)
        return d
        
    ## Build the decks the players chose
    def BuildPlayerDecks(self):
        p1Deck = self.player1DeckChoose.get().split(".")[0] #Get rid of ".txt" from the deck name
        p2Deck = self.player2DeckChoose.get().split(".")[0] #Get rid of ".txt" from the deck name
        
        ## Make sure both players have selected a deck :P
        if p1Deck == "Select a deck" or p2Deck == "Select a deck":
            showwarning(title="Error", message="Ensure both players have selected a deck.")
        else:
            ## This will look for cached cards or get them from the API
            ## Then store them as a list in the Player object
            self.Player1.BuildDeck(deck_name = p1Deck)
            self.Player2.BuildDeck(deck_name = p2Deck)
            
            self.Start()
        
    ## Opening coin flip
    def Start(self):
        ## Clear the GUI ready for the opening coin flip
        self.player1DeckChoose.grid_forget()
        self.player2DeckChoose.grid_forget()
        self.coinChoice1.grid_forget()
        self.coinChoice2.grid_forget()
        self.startButton.grid_forget()
        self.noticeLabel.grid_forget()
        
        self.logBox.insert(tk.END, "{} will choose the coin flip outcome\n".format(self.coinChoiceVar.get()))
        
        ## There must be a more efficient way of doing this...
        ## Grid coin flip options in Player 1 or Player 2 frame according
        ## to who was selected to call it
        if self.coinChoiceVar.get() == "Player 1":
            self.headsButton = tk.Button(self.player1Frame, text="HEADS", command=lambda:self.setCoinChoice("Heads"))
            self.tailsButton = tk.Button(self.player1Frame, text="TAILS", command=lambda:self.setCoinChoice("Tails"))
        else:
            self.headsButton = tk.Button(self.player2Frame, text="HEADS", command=lambda:self.setCoinChoice("Heads"))
            self.tailsButton = tk.Button(self.player2Frame, text="TAILS", command=lambda:self.setCoinChoice("Tails"))
        self.headsButton.grid(row=0, column=0, padx=10, pady=10)
        self.tailsButton.grid(row=1, column=0, padx=10, pady=10)
    
    ## Coin Flip after player selects Heads or Tails
    def setCoinChoice(self, choice):
        coin = FlipACoin() # Returns Heads or Tails using random.choice
        self.logBox.insert(tk.END, "Flipped {}\n".format(coin.upper()))
        self.logBox.insert(tk.END, "{} chose {}\n".format(self.coinChoiceVar.get(), choice.upper()))
        
        ## Remove heads/tails choice buttons
        self.headsButton.grid_forget()
        self.tailsButton.grid_forget()
        
        ## Again... must be a more efficient way to do this
        ## Log who won the coin flip
        if self.coinChoiceVar.get() == "Player 1":
            if choice == coin:
                flipWinner = "Player 1"
                self.logBox.insert(tk.END, "Player 1 won the coin flip and will decide who goes first.\n")
            else:
                flipWinner = "Player 2"
                self.logBox.insert(tk.END, "Player 2 won the coin flip and will decide who goes first.\n")
        else:
            if choice == coin:
                flipWinner = "Player 2"
                self.logBox.insert(tk.END, "Player 2 won the coin flip and will decide who goes first.\n")
            else:
                flipWinner = "Player 1"
                self.logBox.insert(tk.END, "Player 1 won the coin flip and will decide who goes first.\n")
                
        ## Show first/second choices in the player who won's Frame
        if flipWinner == "Player 1":
            self.goFirstButton = tk.Button(self.player1Frame, text="Go First", command=lambda:self.SetFirst(self.Player1))
            self.goSecondButton = tk.Button(self.player1Frame, text="Go Second", command=lambda:self.SetFirst(self.Player2))
        else:
            self.goFirstButton = tk.Button(self.player2Frame, text="Go First", command=lambda:self.SetFirst(self.Player2))
            self.goSecondButton = tk.Button(self.player2Frame, text="Go Second", command=lambda:self.SetFirst(self.Player1))
        self.goFirstButton.grid(row=0, column=0, padx=10, pady=10)
        self.goSecondButton.grid(row=1, column=0, padx=10, pady=10)
        
    def SetFirst(self, player):
        player.goFirst = True
        self.logBox.insert(tk.END, "{} will go first\n".format(player.name))
        
        ## Clear first/second options
        self.goFirstButton.grid_forget()
        self.goSecondButton.grid_forget()
        
        ## Set up
        self.PreSetUp()
        
    def PreSetUp(self):
        self.Player1.ShuffleDeck()
        self.Player1.DrawCards(7)
        
        self.Player2.ShuffleDeck()
        self.Player2.DrawCards(7)
        
        self.InitializePlayerGUIs()
        
        while self.Player1.Mulligan() and self.Player2.Mulligan():
            self.logBox.insert(tk.END, "Both players drew a mulligan and will redraw their hands.\n")
            
            self.ShowHand(Player1, Player2)
            
            self.Player1.ShuffleHandIntoDeck()
            self.Player2.ShuffleHandIntoDeck()
            
            self.Player1.DrawCards(7)
            self.Player2.DrawCards(7)
            
            self.ReloadHandGUI()
        
        if self.Player1.Mulligan():
            self.logBox.insert(tk.END, "Player 1 has drawn a mulligan, and will redraw once Player 2 has finished setting up.\n")
            
        elif self.Player2.Mulligan():
            self.logBox.insert(tk.END, "Player 2 has drawn a mulligan, and will redraw once Player 1 has finished setting up.\n")
        else:
            self.logBox.insert(tk.END, "Both Players: Choose a basic Pokemon to start in the active\n")
            
    
            
    ## Threw these into their own function for readability
    def InitializePlayerGUIs(self):
        ## Hide old player frames
        self.player1Frame.grid_forget()
        self.player2Frame.grid_forget()
        
        ## Player 1 Toolbox
        self.Player1Toolbox = tk.LabelFrame(self.root, text="Player 1 Toolbox")
        self.Player1Toolbox.grid(row=0,column=1,padx=5,rowspan=2)
        
        ## Player 1 Prize Cards
        self.Player1PrizesFrame = tk.LabelFrame(self.Player1Toolbox, text="Prizes ({})".format(len(self.Player1.prizes)))
        self.Player1PrizesFrame.grid(row=0,column=0,padx=5)
        self.Player1PrizesImgLbl = tk.Label(self.Player1PrizesFrame, image=self.cardBackImage)
        self.Player1PrizesImgLbl.grid()
            
        ## Player 1 Deck
        self.Player1DeckFrame = tk.LabelFrame(self.Player1Toolbox, text="Deck ({})".format(len(self.Player1.deck)))
        self.Player1DeckFrame.grid(row=0,column=1,padx=5)
        self.Player1DeckImageLbl = tk.Label(self.Player1DeckFrame, image=self.cardBackImage)
        self.Player1DeckImageLbl.grid()
        
        ## Player 1 Discard Pile
        self.Player1DiscardFrame = tk.LabelFrame(self.Player1Toolbox, text="Discard Pile ({})".format(len(self.Player1.discard)))
        self.Player1DiscardFrame.grid(row=1,column=1,padx=5)
        self.Player1DiscardImageLbl = tk.Label(self.Player1DeckFrame, image=self.frameImage)
        self.Player1DiscardImageLbl.grid()
        
        ## Player 1 Hand
        self.Player1Hand = tk.LabelFrame(self.root, text="Player 1 Hand")
        self.Player1Hand.grid(row=0,column=2,padx=5)
        ## Player 1 hand GUI
        self.Player1HandLabels = []
        col = 0
        ## Loops through the player's hand, creating a label for each card 
        for card in self.Player1.hand:
            cardImage = tk.PhotoImage(file="./images/cards/{}.png".format(card.id))
            cardImageSmaller = cardImage.subsample(3, 3)
            cardImageLbl = tk.Label(self.Player1Hand, image=cardImageSmaller)
            cardImageLbl.image = cardImageSmaller
            cardImageLbl.grid(row=0, column=col)
            ## Binds for Left and Right click
            cardImageLbl.bind("<Button-1>", partial(self.CardClickedHand, card, self.Player1))
            cardImageLbl.bind("<Button-2>", partial(self.ZoomCardWindow, card.id))
            cardImageLbl.bind("<Button-3>", partial(self.ZoomCardWindow, card.id))
            self.Player1HandLabels.append(cardImageLbl)
            col+=1
            
        ## Player 1 Active & Bench
        self.Player1BenchFrame = tk.LabelFrame(self.root, text = "Bench")
        self.Player1BenchFrame.grid(row=1,column=2,padx=5)
        ## Player 1 Active Pokemon
        self.Player1ActiveFrame = tk.LabelFrame(self.Player1BenchFrame, text="Active")
        self.Player1ActiveFrame.grid(row=0, column=0, padx=5)
        self.Player1ActiveLabel = tk.Label(self.Player1ActiveFrame, image=self.frameImage)
        self.Player1ActiveLabel.grid(row=0, column=0)
        ## Generate bench
        self.Player1BenchLabels = []
        
        ##

        ## Player 2 Toolbox
        self.Player2Toolbox = tk.LabelFrame(self.root, text="Player 2 Toolbox")
        self.Player2Toolbox.grid(row=2,column=1,padx=5,rowspan=2)
        
        ## Player 2 Prize Cards
        self.Player2PrizesFrame = tk.LabelFrame(self.Player2Toolbox, text="Prizes ({})".format(len(self.Player2.prizes)))
        self.Player2PrizesFrame.grid(row=0,column=0,padx=5)
        self.Player2PrizesImgLbl = tk.Label(self.Player2PrizesFrame, image=self.cardBackImage)
        self.Player2PrizesImgLbl.grid()
            
        ## Player 2 Deck
        self.Player2DeckFrame = tk.LabelFrame(self.Player2Toolbox, text="Deck ({})".format(len(self.Player2.deck)))
        self.Player2DeckFrame.grid(row=0,column=1,padx=5)
        self.Player2DeckImageLbl = tk.Label(self.Player2DeckFrame, image=self.cardBackImage)
        self.Player2DeckImageLbl.grid()
        
        ## Player 2 Discard Pile
        self.Player2DiscardFrame = tk.LabelFrame(self.Player2Toolbox, text="Discard Pile ({})".format(len(self.Player2.discard)))
        self.Player2DiscardFrame.grid(row=1,column=1,padx=5)
        self.Player2DiscardImageLbl = tk.Label(self.Player2DeckFrame, image=self.frameImage)
        self.Player2DiscardImageLbl.grid()
            
        ## Player 2 Active & Bench
        self.Player2BenchFrame = tk.LabelFrame(self.root, text = "Bench")
        self.Player2BenchFrame.grid(row=2,column=2,padx=5)
        ## Player 2 Active Pokemon
        self.Player2ActiveFrame = tk.LabelFrame(self.Player2BenchFrame, text="Active")
        self.Player2ActiveFrame.grid(row=0, column=0, padx=5)
        self.Player2ActiveLabel = tk.Label(self.Player2ActiveFrame, image=self.frameImage)
        self.Player2ActiveLabel.grid(row=0, column=0)
        
        ## Generate bench
        self.Player2BenchLabels = []
            
        ## Player 2 Hand
        self.Player2Hand = tk.LabelFrame(self.root, text="Player 2 Hand")
        self.Player2Hand.grid(row=3,column=2,padx=5)
        ## Player 2 hand GUI
        self.Player2HandLabels = []
        col = 0
        for card in self.Player2.hand:
            cardImage = tk.PhotoImage(file="./images/cards/{}.png".format(card.id))
            self.cardImageSmaller = cardImage.subsample(3, 3)
            cardImageLbl = tk.Label(self.Player2Hand, image=self.cardImageSmaller)
            cardImageLbl.image = self.cardImageSmaller
            cardImageLbl.grid(row=0, column=col)
            #Binds for left and right click
            cardImageLbl.bind("<Button-1>", partial(self.CardClickedHand, card, self.Player2))
            cardImageLbl.bind("<Button-2>", partial(self.ZoomCardWindow, card.id))
            cardImageLbl.bind("<Button-3>", partial(self.ZoomCardWindow, card.id))
            self.Player2HandLabels.append(cardImageLbl)
            col+=1
            
        
        ##TODO: SHOW HAND TOGGLE FOR 2 IRL PLAYERS
        
    def ReloadHandGUI(self):
        ## Player 1 Hand GUI
        for label in self.Player1HandLabels:
            label.grid_forget()
        col=0
        for card in self.Player1.hand:
            cardImage = tk.PhotoImage(file="./images/cards/{}.png".format(card.id))
            self.cardImageSmaller = cardImage.subsample(3, 3)
            cardImageLbl = tk.Label(self.Player1Hand, image=self.cardImageSmaller)
            cardImageLbl.image = self.cardImageSmaller
            cardImageLbl.grid(row=0, column=col)
            cardImageLbl.bind("<Button-1>", partial(self.CardClickedHand, card, self.Player1))
            cardImageLbl.bind("<Button-2>", partial(self.ZoomCardWindow, card.id))
            cardImageLbl.bind("<Button-3>", partial(self.ZoomCardWindow, card.id))
            self.Player1HandLabels.append(cardImageLbl)
            col+=1
        
        ## Player 2 hand GUI
        for label in self.Player2HandLabels:
            label.grid_forget()
        col=0
        for card in self.Player2.hand:
            cardImage = tk.PhotoImage(file="./images/cards/{}.png".format(card.id))
            self.cardImageSmaller = cardImage.subsample(3, 3)
            cardImageLbl = tk.Label(self.Player2Hand, image=self.cardImageSmaller)
            cardImageLbl.image = self.cardImageSmaller
            cardImageLbl.grid(row=0, column=col)
            cardImageLbl.bind("<Button-1>", partial(self.CardClickedHand, card, self.Player2))
            cardImageLbl.bind("<Button-2>", partial(self.ZoomCardWindow, card.id))
            cardImageLbl.bind("<Button-3>", partial(self.ZoomCardWindow, card.id))
            self.Player2HandLabels.append(cardImageLbl)
            col+=1
            
    def ReloadBenchGUI(self):
        ## Player 1 Active
        if self.Player1.activeP:
            cardImage = tk.PhotoImage(file="./images/cards/{}.png".format(self.Player1.activeP.card.id))
            cardImageSmaller = cardImage.subsample(3, 3)
            self.Player1ActiveLabel.configure(image = cardImageSmaller)
            self.Player1ActiveLabel.image=cardImageSmaller
            self.Player1ActiveLabel.bind("<Button-2>", partial(self.ZoomCardWindow, self.Player1.activeP.card.id))
            self.Player1ActiveLabel.bind("<Button-3>", partial(self.ZoomCardWindow, self.Player1.activeP.card.id))
        ## Player 1 Bench
        for label in self.Player1BenchLabels:
            label.grid_forget()
        col = 1
        for pip in self.Player1.bench:
            cardImage = tk.PhotoImage(file="./images/cards/{}.png".format(pip.card.id))
            cardImageSmaller = cardImage.subsample(3, 3)
            cardImageLbl = tk.Label(self.Player1BenchFrame, image=cardImageSmaller)
            cardImageLbl.image = cardImageSmaller
            cardImageLbl.grid(row=0, column=col)
            ## Binds for left and right click
            cardImageLbl.bind("<Button-1>", partial(self.CardClickedBench, pip, self.Player1))
            cardImageLbl.bind("<Button-2>", partial(self.ZoomCardWindow, pip.card.id))
            cardImageLbl.bind("<Button-3>", partial(self.ZoomCardWindow, pip.card.id))
            self.Player1BenchLabels.append(cardImageLbl)
            col+=1
        
        ## Player 2 Active
        if self.Player2.activeP:
            cardImage = tk.PhotoImage(file="./images/cards/{}.png".format(self.Player2.activeP.card.id))
            cardImageSmaller = cardImage.subsample(3, 3)
            self.Player2ActiveLabel.configure(image = cardImageSmaller)
            self.Player2ActiveLabel.image=cardImageSmaller
            self.Player2ActiveLabel.bind("<Button-2>", partial(self.ZoomCardWindow, self.Player2.activeP.card.id))
            self.Player2ActiveLabel.bind("<Button-3>", partial(self.ZoomCardWindow, self.Player2.activeP.card.id))
        ## Player 2 Bench
        for label in self.Player2BenchLabels:
            label.grid_forget()
        col = 1
        for pip in self.Player2.bench:
            cardImage = tk.PhotoImage(file="./images/cards/{}.png".format(pip.card.id))
            cardImageSmaller = cardImage.subsample(3, 3)
            cardImageLbl = tk.Label(self.Player2BenchFrame, image=cardImageSmaller)
            cardImageLbl.image = cardImageSmaller
            cardImageLbl.grid(row=0, column=col)
            ## Binds for left and right click
            cardImageLbl.bind("<Button-1>", partial(self.CardClickedBench, pip, self.Player2))
            cardImageLbl.bind("<Button-2>", partial(self.ZoomCardWindow, pip.card.id))
            cardImageLbl.bind("<Button-3>", partial(self.ZoomCardWindow, pip.card.id))
            self.Player2BenchLabels.append(cardImageLbl)
            col+=1
            
    def CardClickedHand(self, card, player, event):
        if player.action == {}:
            if not player.activeP and card.supertype == "Pokémon":
                if "Basic" in card.subtypes:
                    pip = PokemonInPlay(card)
                    player.activeP = pip
                    player.hand.remove(card)
                    self.ReloadBenchGUI()
                    self.ReloadHandGUI()
            elif player.activeP and card.supertype == "Pokémon" and len(player.bench)<5:
                if "Basic" in card.subtypes:
                    pip = PokemonInPlay(card)
                    player.bench.append(pip)
                    player.hand.remove(card)
                    self.ReloadBenchGUI()
                    self.ReloadHandGUI()
            elif player.activeP and card.supertype == "Energy":
                self.logBox.insert(tk.END, player.name+" attaching "+card.name+"...")
                player.action.update({"Energy attachment" : card})
    
    ## pip refers to an object of the PokemonInPlay class, which is what all benched and active cards are
    def CardClickedBench(self, pip, player, event):
        if "Energy attachment" in player.action and pip.card.supertype == "Pokémon":
            energy = player.action["Energy attachment"]
            pip.energy.append(energy)
            self.logBox.insert(tk.END, player.name+" attached "+energy.name+" to "+pip.card.name)
            player.action = {}
            player.hand.remove(energy)
            self.ReloadBenchGUI()
            self.ReloadHandGUI()
            
    def ZoomCardWindow(self, cardid, event):
        self.zcw = tk.Toplevel()
        cardImage = tk.PhotoImage(file="./images/cards/{}.png".format(cardid))
        cardImageLabel = tk.Label(self.zcw, image=cardImage, text="Press esc to close")
        cardImageLabel.image=cardImage
        cardImageLabel.grid()
        self.zcw.bind("<Escape>", self.CloseZCW)
        self.zcw.mainloop()
    def CloseZCW(self, event):
        self.zcw.destroy()
        
    def ShowHand(self, *args):
        self.hand_window = tk.Toplvel()
        row=0
        for player in args:
            frame = tk.LabelFrame(self.hand_window, text=player.name)
            frame.grid(row=row, column=0, padx=10, pady=10)
            col=0
            for card in player.hand:
                cardImage = tk.PhotoImage(file="./images/cards/{}.png".format(cardid))
                cardImageSmaller = cardImage.subsample(3, 3)
                cardImageLabel = tk.Label(frame, image=cardImageSmaller)
                cardImageLabel.image=cardImageSmaller
                cardImageLabel.grid(row=0,column=col)
        done_button = tk.Button(frame, text="Done", command=CloseShowHand)
        done_button.grid(row+1, column=0, padx=10, pady=10)
        
        self.hand_window.bind("<Escape>", self.CloseShowHand)
        self.hand_window.mainloop()
            
        
    def Turn(self, number):
        pass
            
            
if __name__ == "__main__":
    root = tk.Tk()
    gamegui = Game(root)
    root.mainloop()
